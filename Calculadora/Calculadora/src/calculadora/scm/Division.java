/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculadora.scm;

import javax.swing.JOptionPane;

/**
 *
 * @author Bryan
 */
public class Division {
    
    
    double numero1;
    double numero2;
    
    Interfaz interfaz;
    
    public Division(double num1,double num2){
        
        this.numero1=num1;
        this.numero2=num2;
        
    }
    
    public double dividir()
    {
        double resultado  = 0.0;
        
        if(numero2!=0)
        {
            resultado = numero1/numero2;
        }
        else
        {
            JOptionPane.showMessageDialog(null,"No está definida la división para '0'");
        }
        return resultado;
    }
    
    
}
